import React from 'react';
import { shallow } from 'enzyme';
import { App } from './';
import Game from '../Game/';
import { i18n } from '../../constants/i18n.en';


describe("<App/> component", () => {
    const wrapper = shallow(<App />);

    it("Should have the application title", () => {
        expect(wrapper.find("header h2").text()).toEqual(i18n.APPLICATION_TITLE);
    });

    it("Should render the <Game /> component", () => {
		expect(wrapper.find(Game).length).toEqual(1);
    });

});
