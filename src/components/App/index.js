import React from 'react';
import Game from '../Game/';
import { i18n } from '../../constants/i18n.en';

import './App.css';

export const App = () => {
    return (
        <div className="App">
            <header className="App-header">
                <h2>{i18n.APPLICATION_TITLE}</h2>
            </header>
            <Game/>
        </div>
    );
}
