import React from 'react';
import PropTypes from 'prop-types';

import './Square.css';

export const Square = (props) => {
    return (<button className="square-button" onClick={props.onClick} disabled={props.isDisabled}>
            {props.value}
        </button>);
}

Square.propTypes = {
    onClick: PropTypes.func.isRequired,
    isDisabled: PropTypes.bool.isRequired,
    value: PropTypes.string
};
