import React from 'react';
import { shallow } from 'enzyme';
import checkPropTypes from 'check-prop-types';
import { Square } from './';

describe("<Square /> component", () => {
    const wrapper = shallow(<Square onClick={jest.fn()} isDisabled={false} />);

    it("Should have interactable square button", () => {
        expect(wrapper.find(".square-button").length).toEqual(1);
    });

    it("Should throw error message if there is no onClick prop", () => {
        const errorMsg = "Failed prop type: The prop `onClick` is marked as required in `<<anonymous>>`, but its value is `undefined`."

        const result = checkPropTypes(Square.propTypes, { onClick: undefined }, "prop", Square.onClick);

        expect(result).toEqual(errorMsg);
    });

    it("Should have the onClick prop of type function", () => {
        const errorMsg = "Failed prop type: Invalid prop `onClick` of type `string` supplied to `<<anonymous>>`, expected `function`."

        const result = checkPropTypes(Square.propTypes, { onClick: "test" }, "prop", Square.onClick);

        expect(result).toEqual(errorMsg);
    });
    
    it("Should have the value prop of type string", () => {
        const errorMsg = "Failed prop type: Invalid prop `value` of type `number` supplied to `<<anonymous>>`, expected `string`."

        const result = checkPropTypes(Square.propTypes, { onClick: jest.fn(), isDisabled: false, value: 1 }, "prop", Square.value);

        expect(result).toEqual(errorMsg);
    });

    it("Should have the isDisabled prop", () => {
        const errorMsg = "Failed prop type: The prop `isDisabled` is marked as required in `<<anonymous>>`, but its value is `undefined`."

        const result = checkPropTypes(Square.propTypes, { onClick: jest.fn(), isDisabled: undefined }, "prop", Square.isDisabled);

        expect(result).toEqual(errorMsg);
    });

    it("Should have the disabled prop of type boolean", () => {
        const errorMsg = "Failed prop type: Invalid prop `isDisabled` of type `string` supplied to `<<anonymous>>`, expected `boolean`."

        const result = checkPropTypes(Square.propTypes, { onClick: jest.fn(), isDisabled: "false" }, "prop", Square.isDisabled);

        expect(result).toEqual(errorMsg);
    });

});
