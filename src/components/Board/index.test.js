import React from 'react';
import { mount } from 'enzyme';
import Board from '../Board/';
import checkPropTypes from 'check-prop-types';
import { Square } from '../Square/';
import { AppConst } from '../../constants/';
import { i18n } from '../../constants/i18n.en';

describe("<Board /> component", () => {
    let wrapper, instance;
    
    const changeActivePlayerMock = () => {
        const activePlayer = (wrapper.props().activePlayer === i18n.PLAYER_X_NAME) ? i18n.PLAYER_O_NAME : i18n.PLAYER_X_NAME;
        wrapper.setProps({
            activePlayer: activePlayer,
            isGameOver: false
        });
    };

    const _fillTheSquares = (squaresList) => {
        squaresList.forEach(square => {
            square.simulate('click');
        });
    };

    const _completeRowByThePlayer = () => {
        const squareButtonList = wrapper.find("ul li .square-button");
        const squareButton1 = squareButtonList.at(1);
        const squareButton2 = squareButtonList.at(2);
        const squareButton3 = squareButtonList.at(3);
        const squareButton4 = squareButtonList.at(4);
        const squareButton5 = squareButtonList.at(5);

        _fillTheSquares([
            squareButton3,
            squareButton1,
            squareButton4,
            squareButton2,
            squareButton5
        ]);
    };

    beforeEach(() => {
        const props = {
            activePlayer: i18n.PLAYER_X_NAME,
            changeActivePlayer: jest.fn(changeActivePlayerMock),
            setTheWinner: jest.fn(),
            gameDrawn: jest.fn()
        };
    
        wrapper = mount(<Board {...props} />);
        instance = wrapper.instance();
    });


    it("Should have 9 squares in the board", () => {
        expect(wrapper.find("ul li").length).toEqual(AppConst.TOTAL_SQUARES);
    });

    it("Should render 9 squares in the board", () => {
        expect(wrapper.find(Square).length).toEqual(AppConst.TOTAL_SQUARES);
    });

    it("Should update the filledSquares list with player 'X' for the first time", () => {
        wrapper.find("ul li button").at(0).simulate("click");

        expect(wrapper.state().filledSquares[0]).toEqual(i18n.PLAYER_X_NAME);
    });

    it("Should display the square text as X on first click of square", () => {
        const squareButton = wrapper.find("ul li .square-button").at(5);

        squareButton.simulate("click");

        expect(squareButton.text()).toEqual(i18n.PLAYER_X_NAME);
    });

    it("Should update the filled square with active player", () => {
        const squareButton = wrapper.find("ul li button").at(4);
        const activePlayer = wrapper.props().activePlayer;

        squareButton.simulate("click");

        expect(wrapper.state().filledSquares[4]).toEqual(activePlayer);
    });

    it("Should throw error message if there is no activePlayer prop", () => {
        const errorMsg = "Failed prop type: The prop `activePlayer` is marked as required in `<<anonymous>>`, but its value is `undefined`."

        const result = checkPropTypes(Board.propTypes, { activePlayer: undefined }, "prop", Board.activePlayer);

        expect(result).toEqual(errorMsg);
    });

    it("Should throw error message if activePlayer prop type is not a string", () => {
        const errorMsg = "Failed prop type: Invalid prop `activePlayer` of type `number` supplied to `<<anonymous>>`, expected `string`."

        const result = checkPropTypes(Board.propTypes, { activePlayer: 12 }, "prop", Board.activePlayer);

        expect(result).toEqual(errorMsg);
    });

    it("Should throw error message if there is no changeActivePlayer prop ", () => {
        const errorMsg = "Failed prop type: The prop `changeActivePlayer` is marked as required in `<<anonymous>>`, but its value is `undefined`."

        const result = checkPropTypes(Board.propTypes, { activePlayer: "X", changeActivePlayer: undefined }, "prop", Board.changeActivePlayer);

        expect(result).toEqual(errorMsg);
    });

    it("Should throw error message if changeActivePlayer prop type is not function", () => {
        const errorMsg = "Failed prop type: Invalid prop `changeActivePlayer` of type `string` supplied to `<<anonymous>>`, expected `function`."

        const result = checkPropTypes(Board.propTypes, { activePlayer: "X", changeActivePlayer: "O" }, "prop", Board.changeActivePlayer);

        expect(result).toEqual(errorMsg);
    });

    it("Should be called changeActivePlayer when the square filled", () => {
        const squareButton = wrapper.find("ul li .square-button").at(3);

        squareButton.simulate("click");

        expect(wrapper.props().changeActivePlayer).toHaveBeenCalled();
    });

    it("Should fill the square with alternate player name", () => {
        const squareButtonAtIndex3 = wrapper.find("ul li .square-button").at(3);

        squareButtonAtIndex3.simulate("click");

        expect(wrapper.state().filledSquares[3]).toEqual(i18n.PLAYER_X_NAME);

        const squareButtonAtIndex2 = wrapper.find("ul li .square-button").at(2);

        squareButtonAtIndex2.simulate("click");

        expect(wrapper.state().filledSquares[2]).toEqual(i18n.PLAYER_O_NAME);
    });

    it("Can be filled only once in the square", () => {
        const squareButton = wrapper.find("ul li .square-button").at(0);
    
        squareButton.simulate("click");
    
        expect(squareButton.is('[disabled]')).toBe(true); 
    });

    it("Should throw error message if there is no setTheWinner prop", () => {
        const errorMsg = "Failed prop type: The prop `setTheWinner` is marked as required in `<<anonymous>>`, but its value is `undefined`."

        const result = checkPropTypes(Board.propTypes, { activePlayer: "X", changeActivePlayer: jest.fn(), setTheWinner: undefined }, "prop", Board.setTheWinner);

        expect(result).toEqual(errorMsg);
    });

    it("Should throw error message if setTheWinner prop type is not function", () => {
        const errorMsg = "Failed prop type: Invalid prop `setTheWinner` of type `string` supplied to `<<anonymous>>`, expected `function`."

        const result = checkPropTypes(Board.propTypes, { activePlayer: "X", changeActivePlayer: jest.fn(), setTheWinner: "test" }, "prop", Board.setTheWinner);

        expect(result).toEqual(errorMsg);
    });

    it("Should call _isAnyRowCompletedByTheActivePlayer", () => {
        const spy = jest.spyOn(instance, "_isAnyRowCompletedByTheActivePlayer");
        instance.forceUpdate();

        const squareButton = wrapper.find("ul li .square-button").at(0);

        squareButton.simulate("click");

        expect(spy).toHaveBeenCalledTimes(1);
    });

    it("Should call _isRowCompletedByTheActivePlayer 3 times", () => {
        const spy = jest.spyOn(instance, "_isRowCompletedByTheActivePlayer");
        instance.forceUpdate();

        const squareButton = wrapper.find("ul li .square-button").at(0);

        squareButton.simulate("click");

        expect(spy).toHaveBeenCalledTimes(AppConst.TOTAL_ROWS);
    });

    it("Should call setTheWinner method when a row completed by the player", () => {
        _completeRowByThePlayer();

        expect(wrapper.props().setTheWinner).toHaveBeenCalled();
    });

    it("Should not proceed the game further, if player wins", () => {
        _completeRowByThePlayer();

        const squaresList = wrapper.find(Square);

        squaresList.forEach(square => {
            expect(square.props().isDisabled).toBeTruthy();
        });
    });

    it("Should not change the active player if game is over", () => {
        _completeRowByThePlayer();

        expect(wrapper.props().activePlayer).toEqual(i18n.PLAYER_X_NAME);
    });

    it("Should call _isAnyColumnCompletedByTheActivePlayer", () => {
        const spy = jest.spyOn(instance, "_isAnyColumnCompletedByTheActivePlayer");
        instance.forceUpdate();

        const squareButton = wrapper.find("ul li .square-button").at(0);

        squareButton.simulate("click");

        expect(spy).toHaveBeenCalledTimes(1);
    });

    it("Should call _isColumnCompletedByTheActivePlayer 3 times", () => {
        const spy = jest.spyOn(instance, "_isColumnCompletedByTheActivePlayer");
        instance.forceUpdate();

        const squareButton = wrapper.find("ul li .square-button").at(0);

        squareButton.simulate("click");

        expect(spy).toHaveBeenCalledTimes(AppConst.TOTAL_COLUMNS);
    });

    it("Player wins when a column completed by the player", () => {
        const squareButtonList = wrapper.find("ul li .square-button");
        const squareButton0 = squareButtonList.at(0);
        const squareButton1 = squareButtonList.at(1);
        const squareButton3 = squareButtonList.at(3);
        const squareButton4 = squareButtonList.at(4);
        const squareButton6 = squareButtonList.at(6);

        _fillTheSquares([
            squareButton0,
            squareButton1,
            squareButton3,
            squareButton4,
            squareButton6
        ]);

        expect(wrapper.state().isGameOver).toBeTruthy();
        expect(wrapper.props().setTheWinner).toHaveBeenCalled();
    });

    it("Should call _isAnyDiagonalCompletedByTheActivePlayer", () => {
        const spy = jest.spyOn(instance, "_isAnyDiagonalCompletedByTheActivePlayer");
        instance.forceUpdate();

        const squareButton = wrapper.find("ul li .square-button").at(0);

        squareButton.simulate("click");

        expect(spy).toHaveBeenCalledTimes(1);
    });

    it("Should call _isDiagonalCompletedByTheActivePlayer 2 times", () => {
        const spy = jest.spyOn(instance, "_isDiagonalCompletedByTheActivePlayer");
        instance.forceUpdate();

        const squareButton = wrapper.find("ul li .square-button").at(0);

        squareButton.simulate("click");

        expect(spy).toHaveBeenCalledTimes(AppConst.TOTAL_DIAGONALS);
    });

    it("Player wins if fills all the squares diagonally", () => {
        const squareButtonList = wrapper.find("ul li .square-button");
        const squareButton0 = squareButtonList.at(0);
        const squareButton1 = squareButtonList.at(1);
        const squareButton4 = squareButtonList.at(4);
        const squareButton5 = squareButtonList.at(5);
        const squareButton8 = squareButtonList.at(8);

        _fillTheSquares([
            squareButton0,
            squareButton1,
            squareButton4,
            squareButton5,
            squareButton8
        ]);
        
        expect(wrapper.state().isGameOver).toBeTruthy();
        expect(wrapper.props().setTheWinner).toHaveBeenCalled();
    });

    it("Should throw error message if there is no gameDrawn prop ", () => {
        const errorMsg = "Failed prop type: The prop `gameDrawn` is marked as required in `<<anonymous>>`, but its value is `undefined`."

        const result = checkPropTypes(Board.propTypes, { activePlayer: "X", changeActivePlayer:jest.fn(), setTheWinner: jest.fn(), gameDrawn: undefined }, "prop", Board.gameDrawn);

        expect(result).toEqual(errorMsg);
    });

    it("Should throw error message if gameDrawn prop type is not function", () => {
        const errorMsg = "Failed prop type: Invalid prop `gameDrawn` of type `string` supplied to `<<anonymous>>`, expected `function`."

        const result = checkPropTypes(Board.propTypes, { activePlayer: "X",  changeActivePlayer:jest.fn(), setTheWinner: jest.fn(), gameDrawn: "O" }, "prop", Board.gameDrawn);

        expect(result).toEqual(errorMsg);
    });

    it("Should be drawn if all the squares filled in the board but not win", () => {
        const squareButtonList = wrapper.find("ul li .square-button");
        const squareButton0 = squareButtonList.at(0);
        const squareButton1 = squareButtonList.at(1);
        const squareButton2 = squareButtonList.at(2);
        const squareButton3 = squareButtonList.at(3);
        const squareButton4 = squareButtonList.at(4);
        const squareButton5 = squareButtonList.at(5);
        const squareButton6 = squareButtonList.at(6);
        const squareButton7 = squareButtonList.at(7);
        const squareButton8 = squareButtonList.at(8);

        _fillTheSquares([
            squareButton0,
            squareButton1,
            squareButton2,
            squareButton6,
            squareButton7,
            squareButton8,
            squareButton3,
            squareButton4,
            squareButton5
        ]);
        
        expect(wrapper.state().isGameOver).toBeTruthy();
        expect(wrapper.props().setTheWinner).not.toHaveBeenCalled();
        expect(wrapper.props().gameDrawn).toHaveBeenCalled();
    });
    
});
