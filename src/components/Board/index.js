import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Square } from '../Square/';
import { AppConst } from '../../constants/';

import './Board.css';

class Board extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filledSquares: [],
            isGameOver: false
        };
        this.filledSquaresCount = 0;
    }
    
    _getSquares = () => {
        const totalSquares = AppConst.TOTAL_SQUARES;
        let squareList = [];

        for (let i = 0; i < totalSquares; i++) {
            squareList.push(<li key={i}>
                <Square onClick={this.fillTheSquare.bind(this, i)} 
                    value={this._getFilledValue(i)} 
                    isDisabled={this._isSquareDisabled(i)} />
            </li>);
        }
        
        return squareList;
    }

    _getFilledValue = (squareIndex) => {
        return (this.state.filledSquares[squareIndex] || "");
    }

    fillTheSquare = (squareIndex) => {
        let filledSquares = this.state.filledSquares;
        
        filledSquares[squareIndex] = this.props.activePlayer;
        this.filledSquaresCount++;
        this.setState({
            filledSquares
        });
        this._checkActivePlayerWinTheGame();
    }

    _isSquareDisabled = (squareIndex) => {
        return (this.state.filledSquares[squareIndex] || this.state.isGameOver) ? true : false;
    }

    _checkActivePlayerWinTheGame = () => {
        let isGameOver = false;
        if (this._isAnyRowCompletedByTheActivePlayer() ||
            this._isAnyColumnCompletedByTheActivePlayer() ||
            this._isAnyDiagonalCompletedByTheActivePlayer()
        ) {
            isGameOver = true;
            this.props.setTheWinner();
        } else if (this._isGameDrawn()) {
            isGameOver = true;
            this.props.gameDrawn();
        } else {
            this.props.changeActivePlayer();
        }

        this.setState({
            isGameOver
        });
    }

    _isAnyRowCompletedByTheActivePlayer = () => {
        const rowStartIndexList = AppConst.ROW_START_INDEXES;
        const totalRows = AppConst.TOTAL_ROWS;
        let isPlayerWin = false;

        for (let rowIndex = 0; rowIndex < totalRows; rowIndex++) {
            let rowStartIndex = rowStartIndexList[rowIndex];
            if (this._isRowCompletedByTheActivePlayer(rowStartIndex)) {
                isPlayerWin = true;
                break;
            }
        }

        return isPlayerWin;
    }

    _isRowCompletedByTheActivePlayer = (rowStartIndex) => {
        const activePlayer = this.props.activePlayer;
        const filledSquares = this.state.filledSquares;

        return (filledSquares[rowStartIndex] === activePlayer &&
            filledSquares[rowStartIndex + 1] === activePlayer &&
            filledSquares[rowStartIndex + 2] === activePlayer);
    }

    _isAnyColumnCompletedByTheActivePlayer = () => {
        const columnStartIndexList = AppConst.COLUMN_START_INDEXES;
        const totalColumns = AppConst.TOTAL_COLUMNS;
        let isPlayerWin = false;

        for(let columnIndex = 0; columnIndex < totalColumns; columnIndex++) {
            let columnStartIndex = columnStartIndexList[columnIndex];
            if(this._isColumnCompletedByTheActivePlayer(columnStartIndex, totalColumns)) { 
                isPlayerWin = true;
                break;
            } 
        }

        return isPlayerWin;
    }

    _isColumnCompletedByTheActivePlayer = (columnStartIndex, totalColumns) => {
        const filledSquares = this.state.filledSquares;
        const activePlayer = this.props.activePlayer;

        return (filledSquares[columnStartIndex] === activePlayer && 
            filledSquares[columnStartIndex + totalColumns] === activePlayer && 
            filledSquares[columnStartIndex + (2 * totalColumns)] === activePlayer);
    }

    _isAnyDiagonalCompletedByTheActivePlayer = () => {
        const totalDiagonals = AppConst.TOTAL_DIAGONALS;
        let isPlayerWin = false;
       
        for(let diagonalIndex = 0; diagonalIndex < totalDiagonals; diagonalIndex++) {
            if(this._isDiagonalCompletedByTheActivePlayer(diagonalIndex)){
                isPlayerWin = true;
                break;
            }
        }
       
        return isPlayerWin;
    }

    _isDiagonalCompletedByTheActivePlayer = (index) => {
        const diagonalIndexList = AppConst.DIAGONAL_INDEXES;
        const filledSquares = this.state.filledSquares;
        const activePlayer = this.props.activePlayer;

        return (filledSquares[diagonalIndexList[index][0]] === activePlayer && 
            filledSquares[diagonalIndexList[index][1]] === activePlayer && 
            filledSquares[diagonalIndexList[index][2]] === activePlayer);
    }

    _isGameDrawn = () => {
        return (this.filledSquaresCount === AppConst.TOTAL_SQUARES);
    }

    render = () => {
        return (<ul className="board">
            {this._getSquares()}
        </ul>);
    }
}

Board.propTypes = {
    activePlayer: PropTypes.string.isRequired,
    changeActivePlayer: PropTypes.func.isRequired,
    setTheWinner: PropTypes.func.isRequired,
    gameDrawn: PropTypes.func.isRequired
};

export default Board;
