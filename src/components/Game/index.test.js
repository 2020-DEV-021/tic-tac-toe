import React from 'react';
import { mount } from 'enzyme';
import Game from './';
import Board from '../Board/';
import { i18n } from '../../constants/i18n.en';

describe("<Game/> component", () => {
    let wrapper, board;

    const _fillTheSquares = (squaresList) => {
        squaresList.forEach(square => {
            square.simulate('click');
        });
    };

    beforeEach(() => {
        wrapper = mount(<Game />);
        board = wrapper.find(Board);    
    });

    it("should have the player X as active player by default", () => {
        expect(wrapper.find("h4").text()).toEqual(i18n.PLAYER + " " + i18n.PLAYER_X_NAME);
    });

    it("Should render the <Board /> component", () => {
        expect(wrapper.find(Board).length).toEqual(1);
    });

    it("Should send the prop 'activePlayer' to the board component", () => {      
        expect(board.props().activePlayer).not.toBeNull();
    });

    it("Should send the prop 'changeActivePlayer' to the board component", () => { 
        expect(board.props().changeActivePlayer).not.toBeNull();
    });

    it("Should send the prop 'changeActivePlayer' of type function", () => {
        expect(board.props().changeActivePlayer).toBeInstanceOf(Function);
    });

    it("Should send the prop 'setTheWinner' of type function", () => {
        expect(board.props().setTheWinner).toBeInstanceOf(Function);
    });

    it("Should display the winner message", () => {
        const squareButtonList = wrapper.find("ul li .square-button");
        const squareButton1 = squareButtonList.at(1);
        const squareButton2 = squareButtonList.at(2);
        const squareButton3 = squareButtonList.at(3);
        const squareButton4 = squareButtonList.at(4);
        const squareButton5 = squareButtonList.at(5);

        _fillTheSquares([ 
            squareButton3,
            squareButton1,
            squareButton4,
            squareButton2,
            squareButton5
        ]);

        expect(wrapper.find(".win-msg").text()).toEqual(i18n.PLAYER + " " + i18n.PLAYER_X_NAME + " " + i18n.WIN_THE_GAME);
    });

    it("Should send the prop 'gameDrawn' of type function", () => {
        expect(board.props().gameDrawn).toBeInstanceOf(Function);
    });

    it("Should display the game drawn message", () => {
        const squareButtonList = wrapper.find("ul li .square-button");
        const squareButton0 = squareButtonList.at(0);
        const squareButton1 = squareButtonList.at(1);
        const squareButton2 = squareButtonList.at(2);
        const squareButton3 = squareButtonList.at(3);
        const squareButton4 = squareButtonList.at(4);
        const squareButton5 = squareButtonList.at(5);
        const squareButton6 = squareButtonList.at(6);
        const squareButton7 = squareButtonList.at(7);
        const squareButton8 = squareButtonList.at(8);

        _fillTheSquares([
            squareButton0,
            squareButton1,
            squareButton2,
            squareButton6,
            squareButton7,
            squareButton8,
            squareButton3,
            squareButton4,
            squareButton5
        ]);

        expect(wrapper.find("p.game-drawn-msg").text()).toEqual(i18n.GAME_DRAWN);
    });

});