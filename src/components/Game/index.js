import React, { Component } from 'react';
import Board from '../Board/';
import { i18n } from '../../constants/i18n.en';

class Game extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activePlayer: i18n.PLAYER_X_NAME,
            winner: null,
            isGameOver: false
        }
    }

    changeActivePlayer = () => {
        this.setState({
            activePlayer: this.getInactivePlayer(),
        });
    }
    
    getInactivePlayer = () => {
        return (this.state.activePlayer === i18n.PLAYER_X_NAME) ? i18n.PLAYER_O_NAME : i18n.PLAYER_X_NAME;
    }
    
    setTheWinner = () => {
        const activePlayer = this.state.activePlayer;
        this.setState({
            winner: activePlayer,
            isGameOver: true
        });
    }

    gameDrawn = () => {
        this.setState({
            isGameOver: true
        });
    }

    _showGameOverMessage = () => {
        return this.state.winner ? 
            (<p className="win-msg">{i18n.PLAYER} {this.state.winner} {i18n.WIN_THE_GAME}</p>)
            :
            (<p className="game-drawn-msg">{i18n.GAME_DRAWN}</p>);

    }

    render = () => {
        return (<div className="game">
            <h4>{i18n.PLAYER} {this.state.activePlayer}</h4>
            <Board activePlayer={this.state.activePlayer} 
                changeActivePlayer={this.changeActivePlayer} 
                setTheWinner={this.setTheWinner} 
                gameDrawn={this.gameDrawn}
            />

            {this.state.isGameOver && this._showGameOverMessage()}
        </div>);
    }
}

export default Game;
