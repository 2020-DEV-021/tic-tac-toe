export const AppConst = {
    TOTAL_SQUARES: 9,
    TOTAL_ROWS: 3,
    ROW_START_INDEXES: [0, 3, 6],
    TOTAL_COLUMNS: 3,
    COLUMN_START_INDEXES: [0, 1, 2],
    TOTAL_DIAGONALS: 2,
    DIAGONAL_INDEXES: [[0, 4, 8], [2, 4, 6]]
};
