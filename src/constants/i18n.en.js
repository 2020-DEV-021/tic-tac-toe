export const i18n = {
    APPLICATION_TITLE: "Tic Tac Toe",
    PLAYER_X_NAME: "X",
    PLAYER: "Player",
    PLAYER_O_NAME: "O",
    WIN_THE_GAME: "win the game",
    GAME_DRAWN: "Game drawn"
};
